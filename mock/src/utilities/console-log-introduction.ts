import { routes } from '../routes/routes'

export function consoleLogIntroduction() {
    console.log('=======================================')
    console.log('👾 👾 👾 👾   👾 👾 👾 👾   👾 👾 👾 👾')
    console.log('👾       👾   👾       👾   👾       👾')
    console.log('👾       👾   👾       👾   👾       👾')
    console.log('👾 👾    👾   👾 👾 👾 👾   👾 👾    👾')
    console.log('         👾                          👾')
    console.log('👾 👾 👾 👾                 👾 👾 👾 👾')
    console.log('=======================================')
    console.log(`\nCompiled ${new Date().toISOString()}`)
    console.log('Server running on http://localhost:3000 🤩')
    console.log('\nAvailable routes:')
    for (const route of routes) {
        console.log(`- ${route.method.toUpperCase()} ${route.path}`)
    }
}
