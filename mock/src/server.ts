import { routes } from './routes/routes'
import { consoleLogIntroduction } from './utilities/console-log-introduction'

const express = require('express')
const jsonServer = require('json-server')
const server = jsonServer.create()

server.use(express.static('public'))
server.use((req: any, res: any, next: any) => {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
})

for (const route of routes) {
    server[route.method]('/api' + route.path, (req: any, res: any) => {
        res.jsonp(route.data)
    })
}

server.listen(3000, () => {
    consoleLogIntroduction()
})
