import { getFeaturedGamesMock } from '../core/featured-games.mock'
import { meCartMock, meMock } from '../core/me.mock'

export const routes = [
    {
        method: 'get',
        path: '/featured-games',
        data: getFeaturedGamesMock
    },
    {
        method: 'get',
        path: '/me',
        data: meMock
    },
    {
        method: 'get',
        path: '/cart',
        data: meCartMock
    },
    {
        method: 'post',
        path: '/cart',
        data: {}
    },
]
