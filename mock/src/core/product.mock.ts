import { getRandomNaturalNumber } from '../../../src/app/core/utilities/numbers.utility'
import { GOGProduct } from '../../../src/app/core/interfaces/product.interface'
import { mockGetRandomUID } from './uid.mock'

export function mockGetProductName(): string {
    return [
        'Cyberpunk 2077',
        'Grand Theft Auto VI',
        'FIFA 2020',
        'WarCraft 3: Reforged',
        'Borderlands 3',
        'Counter-Strike: Global Offensive',
        'Age of Empires 5',
        'Half Life 3',
        'StarCraft III',
        'Quake'
    ][getRandomNaturalNumber(0, 9)]
}

export function mockGetProductPlatform(): string {
    return [
        'WINDOWS',
        'MAC',
        'LINUX',
        'XBOX',
        'PLAYSTATION',
        'STADIA'
    ][getRandomNaturalNumber(0, 5)]
}

export function mockGetProductThumbnail(): string {
    return `http://localhost:3000/assets/images/cover_${getRandomNaturalNumber(0, 9)}.jpg`
}

export function mockGetProductPrice(): number {
    return [
        4.99,
        7.99,
        12.47,
        29.99,
        14.49,
        99.99,
        179.99,
        117.49,
        109.99,
        65.59
    ][getRandomNaturalNumber(0, 9)]
}

export function mockGetProduct(quantity?: number): GOGProduct {
    return {
        id: mockGetRandomUID(),
        name: mockGetProductName(),
        thumbnail: mockGetProductThumbnail(),
        price: mockGetProductPrice(),
        quantity: quantity ? quantity : 0,
        platform: mockGetProductPlatform()
    }
}
