export const meMock = {
    displayName: 'Filip WWWiniarski',
    email: 'filipwiniarski@gmail.com',
    language: 'pl', // I know, should have been an ID - for the sake of easy mocks
    currency: 'pln', // I know,s hould have been an ID - for the sake of easy mocks
    xp: 10432,
    nextLevelXP: 15000,
    level: 5,
    nextReward: 'Loot box of your choice!'
}

export const meCartMock = {
    numberOfItems: 0,
    totalValue: 0,
    totalDiscount: 0,
    idleHours: 0,
    totalXP: 0,
    products: []
}
