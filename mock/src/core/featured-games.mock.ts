import { mockGetProduct } from './product.mock'

export const getFeaturedGamesMock = {
    newReleases: {
        label: 'Latest New Releases',
        routerLink: 'new-releases',
        products: [
            mockGetProduct(),
            mockGetProduct(),
            mockGetProduct(),
            mockGetProduct(),
            mockGetProduct(),
            mockGetProduct()
        ]
    },
    bestsellers: {
        label: 'Bestsellers',
        routerLink: 'bestsellers',
        products: [
            mockGetProduct(),
            mockGetProduct(),
            mockGetProduct(),
            mockGetProduct(),
            mockGetProduct(),
            mockGetProduct()
        ]
    },
    onSale: {
        label: 'On Sale Now',
        routerLink: 'sale',
        products: [
            mockGetProduct(),
            mockGetProduct(),
            mockGetProduct(),
            mockGetProduct(),
            mockGetProduct(),
            mockGetProduct()
        ]
    },
    categories: [
        {
            label: 'Action',
            routerLink: 'action',
            products: [
                mockGetProduct(),
                mockGetProduct(),
                mockGetProduct(),
                mockGetProduct(),
                mockGetProduct(),
                mockGetProduct()
            ]
        },
        {
            label: 'Adventure',
            routerLink: 'adventure',
            products: [
                mockGetProduct(),
                mockGetProduct(),
                mockGetProduct(),
                mockGetProduct(),
                mockGetProduct(),
                mockGetProduct()
            ]
        },
        {
            label: 'Indie',
            routerLink: 'indie',
            products: [
                mockGetProduct(),
                mockGetProduct(),
                mockGetProduct(),
                mockGetProduct(),
                mockGetProduct(),
                mockGetProduct()
            ]
        },
        {
            label: 'RPG',
            routerLink: 'rpg',
            products: [
                mockGetProduct(),
                mockGetProduct(),
                mockGetProduct(),
                mockGetProduct(),
                mockGetProduct(),
                mockGetProduct()
            ]
        }
    ]
}
