/**
 * For each $1 spent user gains $1 * @field XP_FACTOR amount of XP
 * Used to evaluate XP amount per user (generally, in cart)
 */
export const XP_FACTOR = 15
/**
 * @field SCSS_THEME is used to store SCSS variables for ts use
 */
export const COLORS = {
    primary: '#7457ff',
    accent: '#cdff58',
    paragraph: '#3b3b3b',
    warning: '#dd2626',
    success: '#2ad078',
    background: '#ffffff',
    backgroundGrey: '#e1e9f6',
    backgroundDark: '#272e40',
}
