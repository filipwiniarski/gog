import { Component, Injector } from '@angular/core'
import { StatsUtility } from '@core/utilities/stats.utility'
import { ResizeUtility } from '@core/utilities/resize.utility'

@Component({
    selector: 'gog-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    constructor(
        private injector: Injector
    ) {
        /**
         * Create service instances without
         * a need of injection.
         */
        injector.get(StatsUtility)
        injector.get(ResizeUtility)
    }
}
