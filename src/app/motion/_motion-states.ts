/**
 * Use MOTION_STATES to define animation states
 * for ease and consistent use, always define states
 * with initial false state.
 */
export const MOTION_STATES = {
    headerContent: {
        isOpen: false
    },
    cart: {
        isOpen: false
    },
    view: {
        isShrank: false
    }
}
