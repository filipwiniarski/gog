declare const Power3: any

export const MOTION_CONFIG = {
    ease: Power3,
    duration: {
        fast: .1,
        moderate: .23,
        slow: .45,
        turtle: .7
    },
    cartMaxWidth: 450,
    fluidViewBreakpoint: 1400
}
