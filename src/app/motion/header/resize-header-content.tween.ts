import { MOTION_CONFIG } from '@motion/_motion-config'

declare const TweenMax: any

export function tweenResizeHeaderContent(): Promise<void> {
    return new Promise<void>(resolve => {
        const header_content = document.querySelector('#gog-header-content .gog-nav-content-area')
        const header_content_height = header_content.clientHeight
        TweenMax.to('#gog-header-content', MOTION_CONFIG.duration.slow, {
            height: header_content_height + 130,
            ease: MOTION_CONFIG.ease.easeOut,
            onComplete: () => {
                resolve()
            }
        })
        TweenMax.to('#gog-header-content .gog-navigation-content-background', MOTION_CONFIG.duration.slow, {
            opacity: 1
        })
    })
}
