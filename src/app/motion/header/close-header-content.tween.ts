import { MOTION_CONFIG } from '@motion/_motion-config'
import { MOTION_STATES } from '@motion/_motion-states'

declare const TweenMax: any

export function tweenCloseHeaderContent(): Promise<void> {
    return new Promise<void>(resolve => {
        TweenMax.to('#gog-header-content .gog-navigation-content-background', MOTION_CONFIG.duration.slow, {
            opacity: 0.1,
        })
        TweenMax.to('#gog-header-content .gog-nav-content-item', .2, {
            opacity: 0,
            y: -30,
            ease: MOTION_CONFIG.ease.easeOut
        })
        TweenMax.to('#gog-header-content', MOTION_CONFIG.duration.slow, {
            height: 0,
            ease: MOTION_CONFIG.ease.easeOut,
            onComplete: () => {
                MOTION_STATES.headerContent.isOpen = false
                resolve()
            }
        })
    })
}
