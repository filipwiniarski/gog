import { MOTION_CONFIG } from '@motion/_motion-config'

declare const TweenMax: any

export function tweenUndimHeaderContent(): Promise<void> {
    return new Promise<void>(resolve => {
        TweenMax.staggerFromTo('#gog-header-content .gog-nav-content-item', .3, {
            opacity: 0,
            y: -30
        }, {
            opacity: 1,
            y: 0,
            ease: MOTION_CONFIG.ease.easeOut
        }, -.05, () => {
            resolve()
        })
    })
}
