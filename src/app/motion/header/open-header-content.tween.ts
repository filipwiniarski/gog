import { MOTION_CONFIG } from '@motion/_motion-config'
import { MOTION_STATES } from '@motion/_motion-states'
import { tweenResizeHeaderContent } from '@motion/header/resize-header-content.tween'

declare const TweenMax: any

export function tweenOpenHeaderContent(): Promise<void> {
    return new Promise<void>(resolve => {
        tweenResizeHeaderContent()
        TweenMax.to('#gog-header-content .gog-navigation-content-background', MOTION_CONFIG.duration.slow, {
            opacity: 1
        })
        TweenMax.to('#gog-header-content', MOTION_CONFIG.duration.slow, {
            ease: MOTION_CONFIG.ease.easeOut,
            onComplete: () => {
                MOTION_STATES.headerContent.isOpen = true
                resolve()
            }
        })
    })
}
