import { MOTION_CONFIG } from '@motion/_motion-config'
import { MOTION_STATES } from '@motion/_motion-states'

declare const TweenMax: any

export function tweenStretchView(): Promise<void> {
    return new Promise<void>(resolve => {
        TweenMax.to('#app-view', MOTION_CONFIG.duration.slow, {
            className: `-=app-view-shrank`,
            ease: MOTION_CONFIG.ease.easeOut,
            onComplete: () => {
                MOTION_STATES.view.isShrank = false
                resolve()
            }
        })
    })
}
