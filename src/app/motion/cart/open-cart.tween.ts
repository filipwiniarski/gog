import { MOTION_CONFIG } from '@motion/_motion-config'
import { MOTION_STATES } from '@motion/_motion-states'
import { tweenShrinkView } from '@motion/view/shrink-view.tween'

declare const TweenMax: any

export function tweenOpenCart(): Promise<void> {
    return new Promise<void>(resolve => {
        const cart_items = document.querySelectorAll('.gog-cart-item')
        cart_items.forEach(item => {
            TweenMax.fromTo(item, MOTION_CONFIG.duration.slow, {
                x: 100 + (400 * Math.random()),
            },  {
                x: 0,
                ease: MOTION_CONFIG.ease.easeOut
            })
        })
        TweenMax.to('#gog-cart', MOTION_CONFIG.duration.slow, {
            x: '-100%',
            ease: MOTION_CONFIG.ease.easeOut,
            onStart: () => {
                if (window.innerWidth > MOTION_CONFIG.fluidViewBreakpoint)
                    tweenShrinkView()
            },
            onComplete: () => {
                MOTION_STATES.cart.isOpen = true
                resolve()
            }
        })
    })
}
