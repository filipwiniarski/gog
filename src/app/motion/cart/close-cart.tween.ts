import { MOTION_CONFIG } from '@motion/_motion-config'
import { MOTION_STATES } from '@motion/_motion-states'
import { tweenStretchView } from '@motion/view/stretch-view.tween'

declare const TweenMax: any

export function tweenCloseCart(): Promise<void> {
    return new Promise<void>(resolve => {
        TweenMax.to('#gog-cart', MOTION_CONFIG.duration.slow, {
            x: '0%',
            ease: MOTION_CONFIG.ease.easeOut,
            onStart: () => {
                if (window.innerWidth > MOTION_CONFIG.fluidViewBreakpoint)
                    tweenStretchView()
            },
            onComplete: () => {
                MOTION_STATES.cart.isOpen = false
                resolve()
            }
        })
    })
}
