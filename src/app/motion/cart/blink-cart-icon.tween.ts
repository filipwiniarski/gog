import { MOTION_CONFIG } from '@motion/_motion-config'

declare const TweenMax: any

export function tweenBlinkCartIcon() {
    TweenMax.fromTo('.gog-header-cart-blink', MOTION_CONFIG.duration.slow, {
        scale: 1,
        opacity: 1
    }, {
        scale: 2.5,
        opacity: 0,
        ease: MOTION_CONFIG.ease.easeOut
    })
    TweenMax.to('.gog-header-cart-primary', MOTION_CONFIG.duration.fast, {
        scale: .85,
        repeat: 1,
        yoyo: true,
        ease: MOTION_CONFIG.ease.easeInOut
    })
    TweenMax.to('.gog-header-cart span', MOTION_CONFIG.duration.fast, {
        scale: .9,
        repeat: 1,
        yoyo: true,
        ease: MOTION_CONFIG.ease.easeInOut
    })
}
