import { GOGProduct } from '@core/interfaces/product.interface'

export interface GOGFeaturedGamesCategory {
    label: string,
    routerLink: string,
    products: GOGProduct[]
}
