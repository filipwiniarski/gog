import { GOGPlatform } from '@core/enums/platform.enum'

export interface GOGProduct {
    id: string
    name: string
    thumbnail: string
    price: number
    platform: GOGPlatform
    quantity?: number
    discountPrice?: number
    discountAmount?: number
}
