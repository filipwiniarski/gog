export interface GOGMe {
    displayName: string,
    email: string,
    language: string, // I know, should have been an ID - for the sake of easy mocks
    currency: string, // I know, should have been an ID - for the sake of easy mocks
    xp: number,
    nextLevelXP: number,
    level: number,
    nextReward: string
}
