import { GOGFeaturedGamesCategory } from '@core/interfaces/featured-games-category.interface'

export interface GOGFeaturedGames {
    [key: string]: GOGFeaturedGamesCategory | GOGFeaturedGamesCategory[]
}
