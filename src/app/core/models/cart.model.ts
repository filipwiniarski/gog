import { GOGProduct } from '@core/interfaces/product.interface'
import { XP_FACTOR } from '../../GLOBALS'

export class GOGCart {
    numberOfItems: number = 0
    totalValue: number = 0
    totalDiscount: number = 0
    idleHours: number = 0
    totalXP: number = 0
    products: GOGProduct[] = []

    constructor(props?: GOGCart) {
        if (props) {
            this.numberOfItems = props.numberOfItems
            this.totalValue = props.totalValue
            this.totalDiscount = props.totalDiscount
            this.idleHours = props.idleHours
            this.totalXP = props.totalXP
            this.products = props.products
        }
    }

    public addProductToCart(product: GOGProduct): void {
        product.quantity = 1
        if (!this.products.find(_product => _product.id === product.id)) {
            this.products.push(product)
        }
        this.actToCartChange()
    }

    public removeProductFromCart(product: GOGProduct): void {
        product.quantity = 0
        this.products = this.products.filter((_product: GOGProduct) => _product.id !== product.id)
        this.actToCartChange()
    }

    private actToCartChange(): void {
        this.evaluateNumberOfItems()
        this.evaluateTotalValue()
        this.evaluateTotalXP()
    }

    private evaluateTotalValue(): void {
        this.totalValue = this.products
            .map((product: GOGProduct) => product.price)
            .reduce((previous, current) => previous + current, 0)
    }

    private evaluateNumberOfItems(): void {
        this.numberOfItems = this.products
            .map((product: GOGProduct) => product.quantity)
            .reduce((previous, current) => previous + current, 0)
    }

    private evaluateTotalXP(): void {
        this.totalXP = this.products
            .map((product: GOGProduct) => product.price * XP_FACTOR)
            .reduce((previous, current) => previous + current, 0)
    }

}
