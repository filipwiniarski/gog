import { GOGCart } from './cart.model'
import { mockGetProduct } from '../../../../mock/src/core/product.mock'

describe('Cart model', () => {
    it('should create an instance', () => {
        expect(new GOGCart()).toBeTruthy();
    });

    it('should increase product quantity while adding it to the cart', () => {
        const cart = new GOGCart()
        const product_a = mockGetProduct()
        const product_a_quantity = product_a.quantity
        cart.addProductToCart(product_a)
        expect(product_a.quantity).toEqual(product_a_quantity + 1)
    })

    it('should evaluate total cart value', () => {
        const cart = new GOGCart()
        const product_a = mockGetProduct()
        const product_b = mockGetProduct()
        cart.addProductToCart(product_a)
        cart.addProductToCart(product_b)
        expect(cart.totalValue).toEqual((product_a.price * product_a.quantity) + (product_b.price * product_b.quantity))
    })
});
