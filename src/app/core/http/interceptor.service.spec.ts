import { TestBed } from '@angular/core/testing'

import { InterceptorService } from './interceptor.service'

describe('Interceptor Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InterceptorService = TestBed.get(InterceptorService);
    expect(service).toBeTruthy();
  });
});
