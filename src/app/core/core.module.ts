import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { PointerDirective } from './directives/pointer.directive'
import { HoverDirective } from './directives/hover.directive'
import { CurrencySpacePipe } from './pipes/currency-space.pipe'
import { HttpClientModule } from '@angular/common/http'
import { FeaturedGamesService } from '@core/api/featured-games/featured-games.service'
import { ButtonDirective } from './directives/button.directive'
import { StatsUtility } from '@core/utilities/stats.utility'

@NgModule({
    declarations: [
        PointerDirective,
        HoverDirective,
        CurrencySpacePipe,
        ButtonDirective
    ],
    exports: [
        PointerDirective,
        HoverDirective,
        CurrencySpacePipe,
        ButtonDirective
    ],
    providers: [
        FeaturedGamesService,
        StatsUtility
    ],
    imports: [
        HttpClientModule,
        CommonModule
    ]
})
export class CoreModule {
}
