export enum GOGPlatform {
    WINDOWS = 'WINDOWS',
    MAC = 'MAC',
    LINUX = 'LINUX',
    XBOX = 'XBOX',
    PLAYSTATION = 'PLAYSTATION',
    STADIA = 'STADIA'
}
