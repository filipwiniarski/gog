import { Injectable } from '@angular/core'
import { GOGCart } from '@core/models/cart.model'
import { Observable } from 'rxjs'
import { HttpClient } from '@angular/common/http'
import { GOGProduct } from '@core/interfaces/product.interface'
import { tweenBlinkCartIcon } from '@motion/cart/blink-cart-icon.tween'

@Injectable({
    providedIn: 'root'
})
export class CartService {

    public _cart: GOGCart

    get cart(): GOGCart {
        return this._cart
    }

    set cart(cart: GOGCart) {
        this._cart = cart
    }

    constructor(
        private http: HttpClient
    ) {
    }

    public loadCart(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.getCart().subscribe((cart: GOGCart) => {
                this.cart = new GOGCart(cart)
                resolve()
            }, () => {
                reject()
            })
        })
    }

    public addProductToCart(product: GOGProduct): Observable<{}> {
        tweenBlinkCartIcon()
        this.cart.addProductToCart(product)
        return this.http.post<{}>(`/cart`, {
            productId: product,
            quantity: product.quantity
        })
    }

    public removeProductFromCart(product: GOGProduct): Observable<{}> {
        tweenBlinkCartIcon()
        this.cart.removeProductFromCart(product)
        return this.http.delete<{}>(`/cart/${product.id}`)
    }

    private getCart(): Observable<GOGCart> {
        return this.http.get<GOGCart>(`/cart`)
    }
}
