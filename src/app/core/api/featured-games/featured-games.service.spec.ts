import { TestBed } from '@angular/core/testing'

import { FeaturedGamesService } from './featured-games.service'

describe('FeaturedGamesService', () => {
    beforeEach(() => TestBed.configureTestingModule({}))

    it('should be created', () => {
        const service: FeaturedGamesService = TestBed.get(FeaturedGamesService)
        expect(service).toBeTruthy()
    })
})
