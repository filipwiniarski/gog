import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { GOGFeaturedGames } from '@core/interfaces/featured-games.interface'
import { HttpClient } from '@angular/common/http'

@Injectable({
    providedIn: 'root'
})
export class FeaturedGamesService {

    private _featuredGames: GOGFeaturedGames

    get featuredGames(): GOGFeaturedGames {
        return this._featuredGames
    }

    set featuredGames(featuredGames: GOGFeaturedGames) {
        this._featuredGames = featuredGames
    }

    constructor(
        private http: HttpClient
    ) {
    }

    public loadFeaturedGames(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.getFeaturedGames().subscribe((featuredGames: GOGFeaturedGames) => {
                this.featuredGames = featuredGames
                resolve()
            }, () => {
                reject()
            })
        })
    }

    private getFeaturedGames(): Observable<GOGFeaturedGames> {
        return this.http.get<GOGFeaturedGames>(`/featured-games`)
    }
}
