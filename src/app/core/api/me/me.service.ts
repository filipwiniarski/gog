import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { GOGMe } from '@core/interfaces/me.interface'

@Injectable({
    providedIn: 'root'
})
export class MeService {

    private _me: GOGMe

    get me(): GOGMe {
        return this._me
    }

    set me(me: GOGMe) {
        this._me = me
    }

    constructor(
        private http: HttpClient
    ) {
    }

    public loadMe(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.getMe().subscribe((res) => {
                this.me = res
                resolve()
            }, () => {
                reject()
            })
        })
    }

    private getMe(): Observable<GOGMe> {
        return this.http.get<GOGMe>(`/me`)
    }
}
