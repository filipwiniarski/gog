import { Injectable } from '@angular/core'
import { FeaturedGamesService } from '@core/api/featured-games/featured-games.service'
import { MeService } from '@core/api/me/me.service'
import { CartService } from '@core/api/cart/cart.service'

@Injectable({
    providedIn: 'root'
})
export class AppInitializerUtility {

    constructor(
        private featuredGamesService: FeaturedGamesService,
        private meService: MeService,
        private cartService: CartService
    ) {
    }

    public init(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            Promise.all([
                this.featuredGamesService.loadFeaturedGames(),
                this.meService.loadMe(),
                this.cartService.loadCart()
            ]).then(() => {
                resolve()
            }, () => {
                reject()
            })
        })
    }
}
