import { Injectable } from '@angular/core'
import { environment } from '@env/environment'
import * as Stats from 'stats.js'

@Injectable({
    providedIn: 'root'
})
export class StatsUtility {

    constructor() {
        if (!environment.production)
            this.measureFPS()
    }

    private measureFPS(): void {
        const stats0 = new Stats()
        stats0.showPanel(0)
        stats0.dom.style.position = 'absolute'
        const stats1 = new Stats()
        stats1.showPanel(1)
        stats1.dom.style.position = 'absolute'
        const stats2 = new Stats()
        stats2.showPanel(2)
        stats2.dom.style.position = 'absolute'
        document.getElementById('fps-counter').appendChild(stats0.dom)
        document.getElementById('ms-counter').appendChild(stats1.dom)
        document.getElementById('memory-counter').appendChild(stats2.dom)

        function animate() {
            stats0.begin()
            stats0.end()
            stats1.begin()
            stats1.end()
            stats2.begin()
            stats2.end()
            requestAnimationFrame(animate)
        }

        requestAnimationFrame(animate)
    }
}
