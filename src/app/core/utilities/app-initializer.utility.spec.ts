import { TestBed } from '@angular/core/testing'

import { AppInitializerUtility } from './app-initializer.utility'

describe('AppInitializerUtility', () => {
    beforeEach(() => TestBed.configureTestingModule({}))

    it('should be created', () => {
        const service: AppInitializerUtility = TestBed.get(AppInitializerUtility)
        expect(service).toBeTruthy()
    })
})
