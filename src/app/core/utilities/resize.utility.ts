import { Injectable } from '@angular/core'
import { MOTION_CONFIG } from '@motion/_motion-config'
import { tweenStretchView } from '@motion/view/stretch-view.tween'
import { tweenShrinkView } from '@motion/view/shrink-view.tween'
import { MOTION_STATES } from '@motion/_motion-states'

@Injectable({
    providedIn: 'root'
})
export class ResizeUtility {
    constructor() {
        window.addEventListener('resize', () => {
            this.handleWindowResize()
        })
    }
    private handleWindowResize(): void {
        const window_width = window.innerWidth
        if (window_width <= MOTION_CONFIG.fluidViewBreakpoint) {
            if (MOTION_STATES.view.isShrank) tweenStretchView()
        } else {
            if (MOTION_STATES.cart.isOpen) tweenShrinkView()
        }
    }
}
