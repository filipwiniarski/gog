import { Directive, ElementRef, Input, OnInit } from '@angular/core'

@Directive({
    selector: '[gogButton]'
})
export class ButtonDirective implements OnInit {

    @Input('gogButton') style: 'primary' | 'secondary'

    private readonly styleMap: Map<string, string> = new Map<string, string>([
        ['primary', 'gog-button-primary'],
        ['secondary', 'gog-button-secondary']
    ])

    constructor(
        private elementRef: ElementRef
    ) {
    }

    ngOnInit() {
        this.elementRef.nativeElement.className += ` ${this.styleMap.get(this.style)}`
    }

}
