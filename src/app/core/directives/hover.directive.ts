import { Directive, ElementRef, HostListener, Input } from '@angular/core'

@Directive({
    selector: '[gogHover]'
})
export class HoverDirective {

    @Input('gogHover') class: string

    constructor(
        private elementRef: ElementRef
    ) {
    }

    @HostListener('mouseenter')
    private addHoverClass() {
        this.elementRef.nativeElement.className += ` ${this.class}`
    }

    @HostListener('mouseleave')
    private removeHoverClass() {
        this.elementRef.nativeElement.className = this.elementRef.nativeElement.className.replace(` ${this.class}`, '')
    }
}
