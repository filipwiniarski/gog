import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core'

@Directive({
    selector: '[gogPointer]'
})
export class PointerDirective {

    constructor(
        private elementRef: ElementRef,
        private renderer: Renderer2
    ) {
    }

    @HostListener('mouseover')
    private addHoverClass() {
        this.renderer.setStyle(this.elementRef.nativeElement, 'cursor', 'pointer')
    }

}
