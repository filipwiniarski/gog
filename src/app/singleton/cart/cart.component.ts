import { Component, OnInit } from '@angular/core'
import { tweenCloseCart } from '@motion/cart/close-cart.tween'
import { GOGCart } from '@core/models/cart.model'
import { CartService } from '@core/api/cart/cart.service'

@Component({
    selector: 'gog-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

    public readonly cart: GOGCart

    constructor(
        private cartService: CartService
    ) {
        this.cart = this.cartService.cart
    }

    ngOnInit() {
    }

    public closeCart(): void {
        tweenCloseCart()
    }

    public buy(): void {
        alert('More to come in your office (ᴗ ͜ʖ ᴗ)')
    }
}
