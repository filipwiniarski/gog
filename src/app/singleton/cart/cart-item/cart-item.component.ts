import { Component, Input, OnInit } from '@angular/core'
import { GOGProduct } from '@core/interfaces/product.interface'
import { CartService } from '@core/api/cart/cart.service'

@Component({
    selector: 'gog-cart-item',
    templateUrl: './cart-item.component.html',
    styleUrls: ['./cart-item.component.scss']
})
export class CartItemComponent implements OnInit {

    @Input() item: GOGProduct

    constructor(
        private cartService: CartService
    ) {
    }

    ngOnInit() {
    }

    public removeItemFromCart(): void {
        this.cartService.removeProductFromCart(this.item)
    }

}
