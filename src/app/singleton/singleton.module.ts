import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { NavigationComponent } from './navigation/navigation.component'
import { CoreModule } from '@core/core.module'
import { NavigationContentComponent } from './navigation/navigation-content/navigation-content.component'
import { RouterModule } from '@angular/router'
import { NavigationContentAboutComponent } from './navigation/navigation-content/navigation-content-about/navigation-content-about.component'
import { NavigationContentStoreComponent } from './navigation/navigation-content/navigation-content-store/navigation-content-store.component'
import { NavigationContentCommunityComponent } from './navigation/navigation-content/navigation-content-community/navigation-content-community.component'
import { NavigationContentSupportComponent } from './navigation/navigation-content/navigation-content-support/navigation-content-support.component'
import { SharedModule } from '@shared/shared.module'
import { NavigationContentSearchComponent } from './navigation/navigation-content/navigation-content-search/navigation-content-search.component'
import { NavigationContentSettingsComponent } from './navigation/navigation-content/navigation-content-settings/navigation-content-settings.component'
import { ReactiveFormsModule } from '@angular/forms'
import { CartComponent } from './cart/cart.component'
import { CartItemComponent } from './cart/cart-item/cart-item.component'

@NgModule({
    declarations: [
        NavigationComponent,
        NavigationContentComponent,
        NavigationContentAboutComponent,
        NavigationContentStoreComponent,
        NavigationContentCommunityComponent,
        NavigationContentSupportComponent,
        NavigationContentSearchComponent,
        NavigationContentSettingsComponent,
        CartComponent,
        CartItemComponent,
    ],
    exports: [
        NavigationComponent,
        NavigationContentComponent,
        NavigationContentAboutComponent,
        NavigationContentStoreComponent,
        NavigationContentCommunityComponent,
        NavigationContentSupportComponent,
        NavigationContentSearchComponent,
        NavigationContentSettingsComponent,
        CartComponent
    ],
    imports: [
        CoreModule,
        CommonModule,
        RouterModule,
        SharedModule,
        ReactiveFormsModule
    ]
})
export class SingletonModule {
}
