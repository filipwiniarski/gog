import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { NavigationContentSearchComponent } from './navigation-content-search.component'

describe('NavigationContentSearchComponent', () => {
  let component: NavigationContentSearchComponent;
  let fixture: ComponentFixture<NavigationContentSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationContentSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationContentSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
