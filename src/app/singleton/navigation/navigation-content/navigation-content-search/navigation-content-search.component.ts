import { Component, OnInit } from '@angular/core'
import { FormControl } from '@angular/forms'
import { debounceTime, distinctUntilChanged, map, tap } from 'rxjs/operators'
import { Observable } from 'rxjs'

@Component({
    selector: 'gog-navigation-content-search',
    templateUrl: './navigation-content-search.component.html',
    styleUrls: ['../navigation-content.component.scss']
})
export class NavigationContentSearchComponent implements OnInit {

    public search = new FormControl()
    public searchedPhrase$: Observable<string>
    public loading: boolean = false

    constructor() {
    }

    ngOnInit() {
        this.searchedPhrase$ = this.search.valueChanges.pipe(
            distinctUntilChanged(),
            tap(() => this.loading = true),
            debounceTime(350),
            tap(() => this.loading = false),
            map((phrase: string) => phrase.toLowerCase())
        )
    }

}
