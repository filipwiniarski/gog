import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { NavigationContentSettingsComponent } from './navigation-content-settings.component'

describe('NavigationContentSettingsComponent', () => {
  let component: NavigationContentSettingsComponent;
  let fixture: ComponentFixture<NavigationContentSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationContentSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationContentSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
