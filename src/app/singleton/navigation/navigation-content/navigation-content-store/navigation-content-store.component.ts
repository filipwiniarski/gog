import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core'
import { FeaturedGamesService } from '@core/api/featured-games/featured-games.service'
import { takeUntil } from 'rxjs/operators'
import { tweenDimHeaderGames } from '@motion/header/dim-header-games.tween'
import { tweenUndimHeaderGames } from '@motion/header/undim-header-games.tween'
import { timer } from 'rxjs'
import { GOGFeaturedGames } from '@core/interfaces/featured-games.interface'
import { GOGFeaturedGamesCategory } from '@core/interfaces/featured-games-category.interface'

@Component({
    selector: 'gog-navigation-content-store',
    templateUrl: './navigation-content-store.component.html',
    styleUrls: ['../navigation-content.component.scss']
})
export class NavigationContentStoreComponent implements OnInit, OnDestroy {

    private destroy$ = new EventEmitter()
    private switchedCategory$ = new EventEmitter()

    public featuredGames: GOGFeaturedGames
    public selectedCategory: GOGFeaturedGamesCategory

    constructor(
        private featuredGamesService: FeaturedGamesService
    ) {
    }

    ngOnInit() {
        this.switchedCategory$.pipe(
            takeUntil(this.destroy$)
        ).subscribe((category: GOGFeaturedGamesCategory) => {
            if (this.selectedCategory !== category) {
                tweenDimHeaderGames().then(() => {
                    this.selectedCategory = category
                    timer(10).subscribe(() => {
                        tweenUndimHeaderGames()
                    })
                })
            }
        })

        this.featuredGames = this.featuredGamesService.featuredGames
        this.selectedCategory = this.featuredGames.newReleases as GOGFeaturedGamesCategory
        timer(10).subscribe(() => tweenUndimHeaderGames())
    }

    ngOnDestroy() {
        this.destroy$.emit()
    }

    public selectCategory(category): void {
        this.switchedCategory$.emit(category)
    }
}
