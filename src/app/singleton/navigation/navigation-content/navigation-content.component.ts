import { Component, Input, OnInit } from '@angular/core'
import { tweenOpenHeaderContent } from '@motion/header/open-header-content.tween'
import { tweenCloseHeaderContent } from '@motion/header/close-header-content.tween'
import { tweenDimHeaderContent } from '@motion/header/dim-header-content.tween'
import { tweenUndimHeaderContent } from '@motion/header/undim-header-content.tween'
import { Observable, timer } from 'rxjs'
import { tweenResizeHeaderContent } from '@motion/header/resize-header-content.tween'
import { tweenDimHeaderGames } from '@motion/header/dim-header-games.tween'
import { tweenUndimHeaderGames } from '@motion/header/undim-header-games.tween'

@Component({
    selector: 'gog-navigation-content',
    templateUrl: './navigation-content.component.html',
    styleUrls: ['./navigation-content.component.scss']
})
export class NavigationContentComponent implements OnInit {

    /**
     * @field link tells which content should be displayed, but
     * @field displayedLink controls which content is actually displayed
     */

    @Input() linkEmitter$: Observable<string>
    public displayedLink: string

    ngOnInit(): void {
        this.linkEmitter$.subscribe((link: string) => {
            if (this.displayedLink) {
                this.changeTab(link)
            } else if (link) {
                this.openContent(link)
            }
        })
    }

    private changeTab(link: string): void {
        if (!link) {
            this.hideContent()
        } else {
            tweenDimHeaderGames()
            tweenDimHeaderContent().then(() => {
                this.displayedLink = link
                timer(1).subscribe(() => {
                    tweenUndimHeaderGames()
                    tweenUndimHeaderContent()
                    tweenResizeHeaderContent()
                })
            })
        }
    }

    private openContent(link: string): void {
        this.displayedLink = link
        timer(10).subscribe(() => tweenOpenHeaderContent())
        timer(150).subscribe(() => tweenUndimHeaderContent())
    }

    private hideContent(): void {
        tweenCloseHeaderContent().then(() => {
            this.displayedLink = null
        })
    }
}
