import { Component, EventEmitter, OnInit } from '@angular/core'
import { tweenOpenCart } from '@motion/cart/open-cart.tween'
import { tweenCloseHeaderContent } from '@motion/header/close-header-content.tween'
import { CartService } from '@core/api/cart/cart.service'
import { GOGCart } from '@core/models/cart.model'
import { MOTION_STATES } from '@motion/_motion-states'
import { tweenCloseCart } from '@motion/cart/close-cart.tween'

const links = [
    {
        label: 'store',
        routerLink: 'store'
    },
    {
        label: 'about',
        routerLink: 'about'
    },
    {
        label: 'community',
        routerLink: 'community'
    },
    {
        label: 'support',
        routerLink: 'support'
    }
]

interface NavigationLink {
    label: string
    routerLink: string
}

@Component({
    selector: 'gog-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

    public links: NavigationLink[] = links
    public highlightedLink: string
    public highlightedLink$ = new EventEmitter()

    public cart: GOGCart

    constructor(
        private cartService: CartService
    ) {
    }

    ngOnInit() {
        this.cart = this.cartService.cart
        this.highlightedLink$.subscribe((link: string) => {
            this.highlightedLink = link
        })

        document.getElementById('app-view-container')
            .addEventListener('mouseenter', () => {
                this.setHighlightedLink(null)
            })
    }

    public setHighlightedLink(link: string): void {
        this.highlightedLink$.emit(link)
    }

    public clearHighlightedLink(): void {
        this.highlightedLink$.emit(null)
    }

    public toggleCart(): void {
        if (MOTION_STATES.cart.isOpen) {
            tweenCloseCart()
        } else {
            tweenOpenCart()
        }
    }

}
