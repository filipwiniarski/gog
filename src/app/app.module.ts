import { BrowserModule } from '@angular/platform-browser'
import { APP_INITIALIZER, NgModule } from '@angular/core'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { ViewHomeComponent } from '@views/view-home/view-home.component'
import { SingletonModule } from '@singleton/singleton.module'
import { SharedModule } from '@shared/shared.module'
import { CoreModule } from '@core/core.module'
import { environment } from '@env/environment'
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http'
import { InterceptorService } from '@core/http/interceptor.service'
import { AppInitializerUtility } from '@core/utilities/app-initializer.utility'

@NgModule({
    declarations: [
        AppComponent,
        ViewHomeComponent
    ],
    imports: [
        HttpClientModule,
        BrowserModule,
        AppRoutingModule,
        SingletonModule,
        SharedModule,
        CoreModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: InterceptorService,
            multi: true
        },
        {
            provide: APP_INITIALIZER,
            useFactory: initializeGOG,
            deps: [
                AppInitializerUtility
            ],
            multi: true
        },
        {
            provide: 'BASE_API_URL',
            useValue: environment.api
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}

export function initializeGOG(utility: AppInitializerUtility) {
    return (): Promise<any> => {
        return utility.init();
    }
}
