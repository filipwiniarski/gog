import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { CoreModule } from '@core/core.module'
import { DividerComponent } from './divider/divider.component'
import { ProductBoxComponent } from './product-box/product-box.component'
import { ProductPlatformComponent } from './product-platform/product-platform.component'
import { XpBarComponent } from './xp-bar/xp-bar.component'

@NgModule({
    declarations: [
        DividerComponent,
        DividerComponent,
        ProductBoxComponent,
        ProductPlatformComponent,
        XpBarComponent
    ],
    exports: [
        DividerComponent,
        ProductBoxComponent,
        ProductPlatformComponent,
        XpBarComponent
    ],
    imports: [
        CoreModule,
        CommonModule
    ]
})
export class SharedModule {
}
