import { Component, Input, OnInit } from '@angular/core'
import { CartService } from '@core/api/cart/cart.service'
import { GOGProduct } from '@core/interfaces/product.interface'
import { take } from 'rxjs/operators'

@Component({
    selector: 'gog-product-box',
    templateUrl: './product-box.component.html',
    styleUrls: ['./product-box.component.scss']
})
export class ProductBoxComponent implements OnInit {

    @Input() product: GOGProduct

    constructor(
        private cartService: CartService
    ) {
    }

    ngOnInit() {
    }

    public addProductToCart(): void {
        if (this.product.quantity === 0) {
            this.cartService.addProductToCart(this.product).pipe(
                take(1)
            ).subscribe()
        }
    }

}
