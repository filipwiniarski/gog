import { Component, OnInit } from '@angular/core'
import { MeService } from '@core/api/me/me.service'
import { GOGMe } from '@core/interfaces/me.interface'
import { CartService } from '@core/api/cart/cart.service'
import { GOGCart } from '@core/models/cart.model'

@Component({
    selector: 'gog-xp-bar',
    templateUrl: './xp-bar.component.html',
    styleUrls: ['./xp-bar.component.scss']
})
export class XpBarComponent implements OnInit {

    public readonly me: GOGMe
    public readonly cart: GOGCart

    constructor(
        private meService: MeService,
        private cartService: CartService
    ) {
        this.me = this.meService.me
        this.cart = this.cartService.cart
    }

    ngOnInit() {
    }

}
