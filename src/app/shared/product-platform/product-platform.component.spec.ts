import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ProductPlatformComponent } from './product-platform.component'

describe('ProductPlatformComponent', () => {
  let component: ProductPlatformComponent;
  let fixture: ComponentFixture<ProductPlatformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductPlatformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductPlatformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
