import { Component, Input, OnInit } from '@angular/core'
import { GOGPlatform } from '@core/enums/platform.enum'

@Component({
    selector: 'gog-product-platform',
    templateUrl: './product-platform.component.html',
    styleUrls: ['./product-platform.component.scss']
})
export class ProductPlatformComponent implements OnInit {
    @Input() platform: GOGPlatform
    @Input() color: 'white' | 'black' | 'primary'
    public src: string
    ngOnInit() {
        this.src = `./assets/icons/platform-${this.platform.toString().toLowerCase()}.svg`
    }
}
