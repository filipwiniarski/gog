import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ViewHomeComponent } from '@views/view-home/view-home.component'

const routes: Routes = [
    {
        path: '',
        component: ViewHomeComponent
    }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
