export const environment = {
    production: true,
    showStats: false,
    api: '/api'
}
